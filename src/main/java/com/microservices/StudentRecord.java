package com.microservices;

public class StudentRecord {
	
	private long id;
	
	private String uni;
	
	private String cid;

	public StudentRecord(){
		
	}
	
	public StudentRecord(long id, String name, String cid) {
		this.id = id;
		this.uni = name;
		this.cid = cid;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}



	public String getUni() {
		return uni;
	}

	public void setUni(String uni) {
		this.uni = uni;
	}

	public String getCid() {
		return cid;
	}

	public void setCid(String cid) {
		this.cid = cid;
	}

	@Override
	public String toString() {
		return "StudentRecord [id=" + id + ", uni=" + uni + ", course=" + cid
				+ "]";
	}
	
}
