package com.microservices.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import com.microservices.StudentRecord;
import com.microservices.dao.JDBCRecordDAO;

public class JDBCRecordsDAOImpl implements JDBCRecordDAO {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplate;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void insert(StudentRecord record) {

		String sql = "INSERT INTO records "
				+ "(update_id, uni, cid) VALUES (?, ?, ?)";

		jdbcTemplate = new JdbcTemplate(dataSource);

		jdbcTemplate.update(sql, new Object[] { record.getId(),
				record.getUni(), record.getCid() });
	}

	public List<StudentRecord> getAllRecordsByUni(String uni) {
		// TODO Auto-generated method stub

		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "SELECT * FROM records where uni=?";

		List<StudentRecord> records = new ArrayList<StudentRecord>();

		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql,
				new Object[] { uni });
		for (Map row : rows) {
			StudentRecord record = new StudentRecord();
			record.setId(Long.parseLong(String.valueOf(row.get("update_id"))));
			record.setUni((String) row.get("uni"));
			record.setCid(String.valueOf(row.get("cid")));
			records.add(record);
		}

		return records;
	}

	public List<StudentRecord> getAllStudentsByCid(String cid) {

		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "SELECT * FROM records where cid=?";

		List<StudentRecord> records = new ArrayList<StudentRecord>();

		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql,
				new Object[] { cid });
		for (Map row : rows) {
			StudentRecord record = new StudentRecord();
			record.setId(Long.parseLong(String.valueOf(row.get("update_id"))));
			record.setUni((String) row.get("uni"));
			record.setCid(String.valueOf(row.get("cid")));
			records.add(record);
		}

		return records;

	}

	public void addColumn(String colName) {
		String sql = "alter table records add" + colName + " varchar(10)";
		jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.execute(sql);
		return;
	}

	public List<StudentRecord> getLatestUpdates(int num) {
		// TODO Auto-generated method stub
		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "SELECT * FROM records order by update_id desc limit ?";

		List<StudentRecord> records = new ArrayList<StudentRecord>();

		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql,
				new Object[] { num });
		for (Map row : rows) {
			StudentRecord record = new StudentRecord();
			record.setId(Long.parseLong(String.valueOf(row.get("update_id"))));
			record.setUni((String) row.get("uni"));
			record.setCid(String.valueOf(row.get("cid")));
			records.add(record);
		}

		return records;
	}

	public void delete(StudentRecord record) {
		// TODO Auto-generated method stub

	}

	public void deleteRecordsByUni(String uni) {
		// TODO Auto-generated method stub

	}

	public String findCourseByUNI(String uni) {

		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "SELECT cid FROM records WHERE uni = ?";

		String name = (String) jdbcTemplate.queryForObject(sql,
				new Object[] { uni }, String.class);

		return name;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public StudentRecord findByUNI(String uni) {

		String sql = "SELECT * FROM records WHERE uni = ?";

		jdbcTemplate = new JdbcTemplate(dataSource);
		StudentRecord record = (StudentRecord) jdbcTemplate.queryForObject(sql,
				new Object[] { uni }, new BeanPropertyRowMapper(
						StudentRecord.class));

		return record;
	}

	@SuppressWarnings("rawtypes")
	public List<StudentRecord> findAll() {

		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "SELECT * FROM records";

		List<StudentRecord> records = new ArrayList<StudentRecord>();

		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
		for (Map row : rows) {
			StudentRecord record = new StudentRecord();
			record.setId(Long.parseLong(String.valueOf(row.get("update_id"))));
			record.setUni((String) row.get("uni"));
			record.setCid(String.valueOf(row.get("cid")));
			records.add(record);
		}

		return records;
	}

	public void insertBatchSQL(final String sql) {

		jdbcTemplate.batchUpdate(new String[] { sql });

	}

	public void insertBatch1(final List<StudentRecord> records) {

		jdbcTemplate = new JdbcTemplate(dataSource);
		String sql = "INSERT INTO records "
				+ "(update_id, uni, cid) VALUES (?, ?, ?)";

		jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

			public void setValues(PreparedStatement ps, int i)
					throws SQLException {
				StudentRecord record = records.get(i);
				ps.setLong(1, record.getId());
				ps.setString(2, record.getUni());
				ps.setString(3, record.getCid());
			}

			public int getBatchSize() {
				return records.size();
			}
		});
	}

	public void insertBatch2(final String sql) {
		jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.batchUpdate(new String[] { sql });

	}

}
