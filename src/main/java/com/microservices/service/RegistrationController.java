package com.microservices.service;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.microservices.StudentRecord;
import com.microservices.dao.Greeting;
import com.microservices.dao.JDBCRecordDAO;

@RequestMapping("/registration")
@RestController
public class RegistrationController {

    private static final String template = "Hello, %s!";
    private static final String template1 = "Hello, Mr. %s!";
    private final AtomicLong counter = new AtomicLong();
    private final ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    JDBCRecordDAO jdbcRecordDAO = (JDBCRecordDAO) context.getBean("jdbcRecordDAO");
    
    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template, name));
    }
    
    @RequestMapping("/andGreet")
    public Greeting andGreet(@RequestParam(value="name", defaultValue="World") String name) {
        return new Greeting(counter.incrementAndGet(),
                            String.format(template1, name));
    }
    
    @RequestMapping("/student/test")
    public String test(@RequestParam(value="name", defaultValue="World") String name){
		return name;	
    }
    
    @RequestMapping("/lists/all")
    public List<?> getAll(){
    	return jdbcRecordDAO.findAll();
    }
    
    @RequestMapping("/new")
    public StudentRecord newRecord(@RequestParam(value="uni") String uni, @RequestParam(value="cid") String cid){
    	StudentRecord record = new StudentRecord(counter.incrementAndGet(), uni, cid);
    	jdbcRecordDAO.insert(record);
    	return record;
    }
    
    @RequestMapping("/RecordsByUni")
    public List<?> getRecordsByUni(@RequestParam(value="uni") String uni){
    	return jdbcRecordDAO.getAllRecordsByUni(uni);
    }
    
    @RequestMapping("/StudentsByCid")
    public List<?> getStudentsByCid(@RequestParam(value="cid") String cid){
    	return jdbcRecordDAO.getAllStudentsByCid(cid);
    }
    @RequestMapping("/ModifyRecordsTable")
    public List<?> updateRecordsTable(@RequestParam(value="colName") String colName){
    	jdbcRecordDAO.addColumn(colName);
    	return jdbcRecordDAO.getLatestUpdates(5);
    }
    
    @RequestMapping("/GetLatestUpdates")
    public List<?> getLatestRecords(@RequestParam(value="number", defaultValue="10") int num){
    	return jdbcRecordDAO.getLatestUpdates(num);
    }
    
    @RequestMapping("/DeleteRecord")
    public List<?> deleteRecord(@RequestParam(value="uni") String uni, @RequestParam(value="cid") String cid){
    	StudentRecord record = new StudentRecord(counter.incrementAndGet(), uni, cid);
    	jdbcRecordDAO.delete(record);
    	return jdbcRecordDAO.getAllRecordsByUni(uni);
    }
    
    @RequestMapping("/DeleteRecordsByUni")
    public List<?> deleteRecordByUni(@RequestParam(value="uni") String uni){
    	jdbcRecordDAO.deleteRecordsByUni(uni);
    	return jdbcRecordDAO.getAllRecordsByUni(uni);
    } 
    
    
    
}